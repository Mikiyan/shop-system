import dropbox
from datetime import datetime
import time

class Backup():
    
    def __init__(self, db_file):

        self.db_file = db_file
        with open('database/token.txt', 'r') as f:
            self.token = f.readline().split('\n')[0];

    def backup(self):

        # get current date
        current_time = int(time.mktime(datetime.now().timetuple()))
        timestr = str(current_time)

        # read timestamp that written in timestamp.txt
        # if current timestamp and red timestamp is same,
        # the database will not bucked up
        if int(self.read_timestamp()) + 7*24*60*60 > current_time:
            return

        self.write_timestamp(timestr)

        dbx = dropbox.Dropbox(self.token)
        dbx.users_get_current_account()

        with open(self.db_file, 'rb') as f:
            dbx.files_upload(f.read(), '/userdata_{}.db'.format(timestr))

    def read_timestamp(self):
        with open('database/timestamp.txt', 'r') as f:
            return f.readline().split('\n')[0]
        return 0

    def write_timestamp(self, timestamp):
        with open('database/timestamp.txt', 'w') as f:
            f.write(timestamp)



if __name__ == "__main__":
    main()


