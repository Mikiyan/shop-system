# -*-coding: utf-8 -*-

import os
import sqlite3
from contextlib import closing
import time

class DatabaseHandler():

    SUCCEED = 'succeed'

    def __init__(self, db_file):

        if not os.path.exists(db_file):
            self.db_file = db_file
            self.__initialize_database()
        else:
            if not os.path.isfile(db_file):
                print('db_file must be a file.')
            else:
                self.db_file = db_file

    def __initialize_database(self):

        with closing(sqlite3.connect(self.db_file)) as connection:
            cursor = connection.cursor()

            create_accounts = 'create table accounts (id varchar (32), remain_money int)'
            create_history = 'create table history (timestamp int, id varchar (32), charge int)'

            cursor.execute(create_accounts)
            cursor.execute(create_history)

            connection.commit()

    def __charge(self, cursor, id, charge):

        charge_sql = 'update accounts set remain_money = remain_money + ? where id = ?'
        params = (charge, id)
        cursor.execute(charge_sql, params)

        insert_sql = 'insert into history (timestamp, id, charge) values (?, ?, ?)'
        params = (int(time.time()), id, charge)
        cursor.execute(insert_sql, params)

    def check_user_existence(self, id):

        user_exists = False

        with closing(sqlite3.connect(self.db_file)) as connection:
            cursor = connection.cursor()

            select_sql = 'select * from accounts where id = ?'
            params = (id,)
            obj = cursor.execute(select_sql, params)
            try:
                user_exists = len(next(obj)) > 1
            except StopIteration:  # If id was not found.
                pass

        return user_exists

    def add_user(self, id):

        if self.check_user_existence(id):
            return 'User {} exists.'

        with closing(sqlite3.connect(self.db_file)) as connection:
            cursor = connection.cursor()

            insert_sql = 'insert into accounts(id, remain_money) values (?, ?)'
            params = (id, 0)
            cursor.execute(insert_sql, params)

            connection.commit()

        return self.SUCCEED

    def delete_user(self, id):

        if not self.check_user_existence(id):
            return 'User {} does not exist.'

        with closing(sqlite3.connect(self.db_file)) as connection:
            cursor = connection.cursor()

            delete_sql = 'delete from accounts where id = ?'
            cursor.execute(delete_sql, (id,))

            connection.commit()

        return self.SUCCEED

    def charge(self, id, amount):

        if amount <= 0:
            return 'Invalid amount.'

        if not self.check_user_existence(id):
            return 'User {} does not exist.'

        with closing(sqlite3.connect(self.db_file)) as connection:
            cursor = connection.cursor()

            self.__charge(cursor, id, amount)

            connection.commit()

    def pay(self, id, amount):

        if amount <= 0:
            return 'Invalid amount.'

        if not self.check_user_existence(id):
            return 'User {} does not exist.'

        with closing(sqlite3.connect(self.db_file)) as connection:
            cursor = connection.cursor()

            self.__charge(cursor, id, -amount)

            connection.commit()

    def get_remain(self, id):

        if not self.check_user_existence(id):
            return 'User {} does not exist.'

        amount = 0
        with closing(sqlite3.connect(self.db_file)) as connection:
            cursor = connection.cursor()

            select_sql = 'select remain_money from accounts where id = ?'
            params = (id,)
            res = cursor.execute(select_sql, params)
            try:
                amount = next(res)[0]
            except:
                pass

            connection.commit()

        return amount

    def print_database(self):

        with closing(sqlite3.connect(self.db_file)) as connection:
            cursor = connection.cursor()

            select_accounts_sql = 'select * from accounts'
            select_histroy_sql = 'select * from history'

            table_accounts = cursor.execute(select_accounts_sql)
            print('table: accounts')
            for row in table_accounts:
                print(row)

            table_history = cursor.execute(select_histroy_sql)
            print('table: history')
            for row in table_history:
                print(row)

if __name__ == "__main__":
    handler = DatabaseHandler('database/userdata.db')
    handler.print_database()

