# -*- coding: utf-8 -*
import os
import kivy

from kivy.app import App
from kivy.core.window import Window
from kivy.factory import Factory

from kivy.uix.boxlayout import BoxLayout

from kivy.lang import Builder

Builder.load_file('home.kv')
Builder.load_file('pay_confirm.kv')
Builder.load_file('charge_confirm.kv')
Builder.load_file('complete.kv')
Builder.load_file('error.kv')
Builder.load_file('check_remain.kv')

from home import home
from pay_confirm import pay_confirm
from charge_confirm import charge_confirm
from complete import complete
from error import error
from check_remain import check_remain

import threading
import time

from database.database_handler import DatabaseHandler
from nfc_reader.id_reader import IDReader 
from database.backup import Backup


class DelayedTrigger():

    def __init__(self, func, delay):
        self.func = func
        self.delay = delay
        thread = threading.Thread(target=self.trigger)
        thread.start()
    def trigger(self):
        time.sleep(self.delay)
        self.func()

class MainRoot(BoxLayout):

    home = None
    pay_confirm = None
    error = None
    charge_confirm = None
    complete = None
    check_remain = None

    def __init__(self, **kwargs):

        self.home = Factory.home()
        self.pay_confirm = Factory.pay_confirm()
        self.charge_confirm = Factory.charge_confirm()
        self.complete = Factory.complete()
        self.error = Factory.error()
        self.check_remain = Factory.check_remain()

        self.db_handler = DatabaseHandler('database/userdata.db')
        self.id_reader = IDReader()
        self.backup = Backup('database/userdata.db')

        super(MainRoot, self).__init__(**kwargs)
        
        self.amount = 100
        
        self.show_home()

    def pay():
        pass
        # check users

    def add_payment(self, amount):
        self.amount += amount
        if self.amount < 0:
            self.amount = 0
        self.home.ids['pay_label'].text = 'Pay\n¥{}'.format(self.amount)

    def show_home(self):
        self.amount = 100
        self.clear_widgets()
        self.add_widget(self.home)
        self.home.ids['pay_label'].text = 'Pay\n¥{}'.format(self.amount)
        
    
    def confirm_payment(self):

        thread = threading.Thread(target=self.pay_wait4id)
        thread.start()
        
        self.show_pay_confirm('Pay ¥{}'.format(self.amount))

    # wait for id, if id is touched then func will be called.
    def pay_wait4id(self):
        print('waiting id')
        timed_out = self.id_reader.wait(self.pay_reader_callback)
        if timed_out:
            print('timed out')
            self.show_home()

        # take a backup to dropbox
        self.backup.backup()

    def pay_reader_callback(self, ID):
        print(ID)
        if not ID:
            self.show_home()
            return

        remain_money = 0
        if self.db_handler.check_user_existence(ID):
            remain_money = self.db_handler.get_remain(ID)

        before_money = remain_money
        after_money = remain_money - self.amount
        update_text = '¥{} -> ¥{}'.format(before_money, after_money)
        if after_money >= 0:
            self.db_handler.pay(ID, self.amount)
            self.show_complete('Thanks for your purchace!', update_text)
        else:
            self.show_error(update_text)

    def confirm_charge(self):
        thread = threading.Thread(target=self.charge_wait4id)
        thread.start()
        self.show_charge_confirm()

    def charge_wait4id(self):
        print('waiting id')
        timed_out = self.id_reader.wait(self.charge_reader_callback)
        if timed_out:
            print('timed_out')
            self.show_home()

    def charge_reader_callback(self, ID):
        print(ID)
        if not ID:
            self.show_home()
            return

        if not self.db_handler.check_user_existence(ID):
            self.db_handler.add_user(ID)

        before_money = self.db_handler.get_remain(ID)
        after_money = before_money + 1000
        update_text = '¥{} -> ¥{}'.format(before_money, after_money)
        self.db_handler.charge(ID, 1000)
        self.show_complete('Complete to charge!', update_text)


    def query_remain(self):
        def wait4id():
            print('waiting id')
            timed_out = self.id_reader.wait(self.check_remain_callback)
            if timed_out:
                print('timed_out')
                self.show_home()

        thread = threading.Thread(target=wait4id)
        thread.start()
        self.show_check_remain('Touch your ID.')

    def check_remain_callback(self, ID):
        print(ID)
        if not ID:
            self.show_home()
            return

        remain_money = self.db_handler.get_remain(ID)
        self.show_complete('Your remain money is', '¥{}'.format(remain_money))

    def show_pay_confirm(self, notice):
        self.clear_widgets()
        self.add_widget(self.pay_confirm)
        self.pay_confirm.ids['notice'].text = notice

    def show_charge_confirm(self):
        self.clear_widgets()
        self.add_widget(self.charge_confirm)

    def show_complete(self, message, notice):
        self.clear_widgets()
        self.add_widget(self.complete)
        self.complete.ids['message'].text = message
        self.complete.ids['notice'].text = notice
        
        DelayedTrigger(self.show_home, 4.0)


    def show_error(self, notice):
        self.clear_widgets()
        self.add_widget(self.error)
        self.error.ids['notice'].text = notice
        
        DelayedTrigger(self.show_home, 4.0)

    def show_check_remain(self, message):
        self.clear_widgets()
        self.add_widget(self.check_remain)
        self.check_remain.ids['message'].text = message

class MainApp(App):
    def __init__(self, **kwargs):
        super(MainApp, self).__init__(**kwargs)
        self.title = 'Switching test'



if __name__ == "__main__":
    MainApp().run()
