import time
import nfc

class IDReader():

    def __init__(self):
        self.receiver = None
        self.clf = None

    def on_connect(self, tag):


        ID = None
        attributes = str(tag).split(' ')
        for attr in attributes:
            if attr.startswith('ID='):
                ID = attr.split('ID=')[1]
        self.clf.close()
        self.receiver(ID)
        self.timed_out = False

    def wait(self, receiver):
        self.receiver = receiver
        self.timed_out = True

        self.clf = nfc.ContactlessFrontend('usb')
        #print(dir(self.clf))
        if self.clf:
            started = time.time()
            self.clf.connect(rdwr={
                'on-connect': self.on_connect
                }, terminate=lambda: time.time() - started > 5)
        self.clf.close()

        return self.timed_out
        
if __name__ == '__main__':
    reader = IDReader()

    def receiver(i):
        print(i)

    reader.wait(receiver)

